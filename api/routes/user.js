const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/userModel");

router.post("/signup", (req, res, next) => {
  console.log(req.body.password);
  console.log(req.body.email);
  User.find({ email: req.body.email })
    .exec()
    .then((user) => {
      if (user.length >= 1) {
        return res.status(422).json({
          message: "Email already exist",
        });
      } else {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err,
            });
          } else {
            console.log("hashed pass => " + hash);
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: req.body.email,
              password: hash,
            });
            user
              .save()
              .then((result) => {
                console.log(result);
                res.status(201).json({
                  message: "User Created",
                });
              })
              .catch((err) => {
                res.status(500).json({
                  message: "User Creation failed ",
                  error: err,
                });
              });
          }
        });
      }
    })
    .catch();
});

router.delete("/:userId", (req, res, nest) => {
  console.log(req.params.userId);
  User.deleteOne({ _id: req.params.userId })
    .exec()
    .then((result) => {
      return res.status(200).json({
        message: "User deleted",
      });
    })
    .catch((err) => {
      return res.status(400).json({
        message: "Could not find user",
      });
    });
});

router.post("/login", (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then((user) => {
      console.log(user);
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth Failed ",
        });
      }
      bcrypt.compare(req.body.password, user[0].password, (err, result) => {
        console.log("User 0 => " + user[0]);
        if (err) {
          return res.status(401).json({
            message: "Auth Failed",
          });
        }
        if (result) {
          const token = jwt.sign(
            {
              email: user[0].email,
              userId: user[0]._id,
            },
            process.env.JWT_KEY,
            {
              expiresIn: "1h",
            }
          );
          console.log("Result ==> " + result);
          return res.status(200).json({
            message: "Auth Successful",
            token: token,
          });
        } else {
          res.status(401).json({
            message: "Auth Failed",
          });
        }
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: "User Creation failed ",
        error: err,
      });
    });
});

router.get("/", (req, res, next) => {
  User.find()
    .exec()
    .then((users) => {
      res.status(200).json({
        users: users,
      });
    })
    .catch((err) => {
      res.status(500).json({
        message: err,
      });
    });
});

module.exports = router;
